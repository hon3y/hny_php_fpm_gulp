#!/bin/bash

if [ ! -f .gulp_initialized ]; then                                                                                                                                                                                    
    echo "Initializing gulp container"                                                                                                                                                                                 

    npm link gulp \
    && npm link gulp-sass \
    && npm link gulp-clean-css \
    && npm link gulp-autoprefixer \
    && npm link gulp-wrap \
    && npm link gulp-tap \
    && npm link gulp-file-include \
    && npm link gulp-concat \
    && npm link gulp-rename \
    && npm link gulp-uglify \
    && npm link gulp-util \
    && npm link gulp-debug

    touch .gulp_initialized                                                                                                                                                                                            
fi                                                                                                                                                                                                                

exec "$@"